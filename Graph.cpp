
#include <iostream>
#include <vector>
#include <cstring>

#include <algorithm>
#include "Graph.h"
#include <cmath>
#include <fstream>



vector<int> findLocationspace(string sample,char findIt)
{   
    vector<int> characterLocations;
    for(int i =0; i < sample.size(); i++){
        if(sample[i] == findIt)
            {characterLocations.push_back(i);
            }
   }
    return characterLocations;
}




/***********************************************************/
/*                      constucors                         */
/***********************************************************/ 
Graph::Graph() {};

Graph::Graph(Node *n):initialNode(n){
}

Graph::~Graph() = default;

/////////////////////       constructor from a file    ///////////////////////////// 
 
Graph::Graph(string file){

ifstream flux(file);  //Ouverture du fichier 
if (flux){

    string ligne;
    vector<int> location;
    map<string, int> map_nodes; //map for name and the num of nodes in g.states 
    
     // Node * n = new Node(1381,'ag_1381');
    
       
    
    
while(getline(flux, ligne))//read line by line 
{
        
      location= findLocationspace(ligne,' ');
      if (location.size()>2) //no last sentence  
      {
	string ag_source = ligne.substr(0,location[0]);
	int n_ag_source= std::stoi(ag_source.substr(3,ag_source.length()-3));
	
	string ag_fin = ligne.substr(location[1]+1,location[2]-location[1]-1); 
        int n_ag_fin= std::stoi(ag_fin.substr(3,ag_source.length()-2));
        
	string transition = ligne.substr(location[6]+2,ligne.length()-location[6]-4)+"_"; 

	if (map_nodes.find(ag_source)== map_nodes.end()){ // node doesn't exist 
		Node * n = new Node(n_ag_source,ag_source);
		states.push_back(n);
		map_nodes.insert(pair (ag_source,states.size()-1));
		//cout<<"add to map "<<ag_source<<"---"<<states.size()-1<<endl; 		
	}
	if (map_nodes.find(ag_fin)== map_nodes.end()){ // node doesn't exist 
		Node * n = new Node(n_ag_fin,ag_fin);
		states.push_back(n);
		map_nodes.insert(pair (ag_fin,states.size()-1));
		//cout<<"add to map "<<ag_fin<<"---"<<states.size()-1<<endl; 
	}

        trNode t;
        t.first=transition;  

        //ajouter trnode  transitions(map)
        if (transitions.find(t.first) == transitions.end())
         { // add new transition with number tr 
         
        	transitions.insert({t.first,transitions.size()});
        	//cout <<"ajouter une trnode " << t.first<<" avec un num "<<transitions.size()<< endl;
            }
          // cout<<"la position de source "<<map_nodes[ag_source]<<endl; 
          //cout<<"la position de a fin "<<map_nodes[ag_fin]<<endl; 
         t.second=states[map_nodes[ag_fin]];
         states[map_nodes[ag_source]]->getSuccessors().push_back(t);

           
        
       }
       
   // ad the initial node
  std::size_t found = ligne.find("[initialstate]");
  if (found!=std::string::npos) {
  cout<<"c'est l'intialisation "<<map_nodes[ligne.substr(0,found)] <<endl;
  initialNode = states[map_nodes[ligne.substr(0,found)]];
}
       

}
     }

 
else
{
    cout << "ERREUR while opening the file !" << endl;
}


}


/***********************************************************/
/*                      compute Language                   */
/***********************************************************/ 
pair<vector<transSeqLang>, vector<int>> Graph::computeLanguage(Node* n){

    string regEx="";
    string circuits ="";
    int countCircuits = 0;
 	//cout<<" ---computeLanguage-----"<<n->getName()<<endl;
    if(n->getSuccessors().empty())
        {
        //    cout << n->getName()<<" do not have successors" << endl;
            return {{{"",NULL}},{1}};
        }
    else if(find(stack.begin(), stack.end(), n->getId()) != stack.end())
        {
          // cout << n->getName()<<" est dans le stack! " << endl;
            return {{{"", n}},{1}};
        }
    else if(find(tas.begin(), tas.end(), n->getId()) != tas.end())
        {
            //cout << n->getName()<<" est dans le tas!" << endl;
            return getLanguage(n, true);
        }
    stack.push_back(n->getId());
     // cout << n->getName()<<" ajout stack!" << endl;
    tas.insert(n->getId());
      //cout << n->getName()<<" ajout tas!" << endl;
    for(trNode trn: n->getSuccessors()){
                auto trLanNbPaths = computeLanguage(trn.second);
                int i=0;
                for(transSeqLang trLan : trLanNbPaths.first) {
               // if(trLan.second==NULL)cout<<"le language de "<<trn.second->getName()<<" est: "<<trLan.first<<" et Null"<<endl;
                //else cout<<"le language de "<<trn.second->getName()<<" est: "<<trLan.first<<" et "<<trLan.second->getName()<<endl;
                    n->getLanguages().push_back({trn.first + trLan.first, trLan.second});
                    n->nbPaths.push_back(trLanNbPaths.second[i]);
                    i++;
                }
        }


    auto it = n->getLanguages().begin();
    int i =0;
    int countNbPaths = 1;

    while (it != n->getLanguages().end())
    {    
  /*  if (it->second!=NULL)cout<<"le language  -- de "<<n->getName()<<" -- est--"<<it->first<<" -- "<<it->second->getName()<<endl;
    else cout<<"le language  -- de "<<n->getName()<<" -- est--"<<it->first<<" -- Null"<<endl;*/
        if (it->second!=NULL && it->second->getId()==n->getId()) //une boucle
        {
            countCircuits++;
            circuits += "("+it->first+")*";
            it = n->getLanguages().erase(it);
            countNbPaths *= n->nbPaths[i] + 1;
            n->nbPaths.erase(n->nbPaths.begin()+i);
        }
        else {
            ++it;
            i++;
        }
    }

    if(countCircuits>1)circuits = "("+circuits+")*";


    if(countCircuits>0){
        if(n->getLanguages().size()==0)
            {
                n->getLanguages().push_back({circuits, NULL});
                // A tester
                //n->nbPaths.push_back(pow(2, countCircuits));
                n->nbPaths.push_back(countNbPaths);
            }
        else {
            int i =0;
            for (transSeqLang &trseqlg: n->getLanguages()) {
                trseqlg.first = circuits + trseqlg.first;
                //n->nbPaths[i] *= pow(2, countCircuits);
                n->nbPaths[i] *= countNbPaths;
                i++;
            }
        }
    }
    stack.pop_back();

    //To display final regEx
       if(n->getId()==initialNode->getId()){
        for(int &i : n->nbPaths)
            {this->nbPaths += i;}
         if(n->getLanguages().size() == 0){
             regEx = circuits;
             n->getLanguages().push_back({circuits, NULL});
         }
         else {
             for(transSeqLang lg: n->getLanguages()){
                regEx = regEx + lg.first + "+\n";
             }
             regEx.pop_back();
             regEx.pop_back();
         }
        cout << "Regex: " << regEx   <<endl;
        cout<<endl;
    }


    if(n->getId() == 0)
        cout << "nb Paths: " << this->nbPaths << endl;

    return {n->getLanguages(), n->nbPaths};

};



pair<vector<transSeqLang>, vector<int>> Graph::getLanguage(Node* n, bool firstCall){

    string circuits ="";
    int countCircuits = 0;
    vector<transSeqLang> toAdd;

    //It's an optimisation we can uncomment it but we affect the nbPaths count
//    if(firstCall){
//        stack.push_back(n->getId());
//        tas.insert(n->getId());
//
//        auto it = n->getLanguages().begin();
//        while(it != n->getLanguages().end()){
//
//            if(it->second != NULL && (std::find(stack.begin(), stack.end(), it->second->getId()) == stack.end())){
//                for(transSeqLang trLan: getLanguage(it->second,false).first){
//                    toAdd.push_back({it->first+trLan.first, trLan.second});
//                }
//                it = n->getLanguages().erase(it);
//
//            }
//            else {
//                ++it;
//            }
//        }
//
//
//        n->getLanguages().insert(std::end(n->getLanguages()), std::begin(toAdd), std::end(toAdd));
//    }
//    else{

        if(n->getSuccessors().empty())
            {
                return {{{"",NULL}},{1}};
            }
        else if(find(stack.begin(), stack.end(), n->getId()) != stack.end())
            {
                return {{{"", n}},{1}};
            }
        n->getLanguages().clear();
        n->nbPaths.clear();
        stack.push_back(n->getId());
        tas.insert(n->getId());
        for(trNode trn: n->getSuccessors()){
            auto trLanNbPaths = computeLanguage(trn.second);
            int i=0;
            for(transSeqLang trLan : trLanNbPaths.first) {
                n->getLanguages().push_back({trn.first + trLan.first, trLan.second});
                n->nbPaths.push_back(trLanNbPaths.second[i]);
                i++;
            }
        }
//    }


    auto it = n->getLanguages().begin();
    int i =0;
    int countNbPaths = 1;
    while (it != n->getLanguages().end())
    {
        if (it->second!=NULL && it->second->getId()==n->getId())
        {
            countCircuits++;
            circuits += "("+it->first+")*";
            it = n->getLanguages().erase(it);
            countNbPaths *= n->nbPaths[i] + 1;
            n->nbPaths.erase(n->nbPaths.begin()+i);
        }
        else {
            ++it;
            i++;
        }
    }


    if(countCircuits>1)circuits = "("+circuits+")*";
    if(countCircuits>0){
        int i=0;
        for(auto iter=n->getLanguages().begin(); iter!=n->getLanguages().end();++iter){
            iter->first = circuits + iter->first;
            n->nbPaths[i] *= countNbPaths;
            i++;
        }
    }
    stack.pop_back();




    return {n->getLanguages(), n->nbPaths};
}

string RandomString(int ch)
{   //number of alphabetic characters
    const int ch_max = 26;
    char alpha[ch_max] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                           'h', 'i', 'j', 'k', 'l', 'm', 'n',
                           'o', 'p', 'q', 'r', 's', 't', 'u',
                           'v', 'w', 'x', 'y', 'z' };
    string result = "";
    for (int i = 0; i<ch; i++)
        result = result + alpha[rand() % ch_max];

    return result;
}



// NOV: number of vertices
void Graph::generateRandomGraph(int NOV ){
    int nbTransitions = 0;
    for(int i=0; i<NOV;i++){
        Node * n = new Node(i);
        this->states.push_back(n);
    }
    trNode t;
    int nbSuccessors;
    int nbGen = 1;
    for(int i=0; i<NOV;i++){
//        srand(time(NULL));
//        if(i < NOV - NOV*10/100)nbSuccessors =rand() % ((3 - 1) + 1 ) +1;
//        else nbSuccessors = rand()%1;
        nbSuccessors = rand() % ((3 - 1) + 1 ) +1;
        if(nbGen + nbSuccessors >=NOV )break;
        for(int j=0; j<nbSuccessors;j++) {
            t.first = RandomString(2).append("_");
            if (transitions.find(t.first) == transitions.end()) {
                transitions.insert({t.first, nbTransitions});
                nbTransitions++;
            }
            t.second = this->states[nbGen];
            nbGen++;
            this->states[i]->getSuccessors().push_back(t);
            if (i!=0 && (nbGen+1) % (20 * NOV / 100) == 0) {
                t.first = RandomString(2).append("_");
                if (transitions.find(t.first) == transitions.end()) {
                    transitions.insert({t.first, nbTransitions});
                    nbTransitions++;
                }
                t.second = this->states[rand() % i];
                this->states[i]->getSuccessors().push_back(t);
            }
        }
    }
    this->initialNode = this->states[0];
}

void Graph::printGraph() {
    cout<<"\nThe generated graph is: ";
    for(int i=0; i<this->states.size(); i++){
        cout<<"\n\t"<<i<<"-> { ";
        for(auto v :this->states[i]->getSuccessors()){
            cout << v.first << "," << v.second->getId() << "	";
        }
        cout<<" }";
    }
    cout << endl;
}

void Graph::printGraphFile() { 
 for(int i=0; i<this->states.size(); i++){
        cout<<"\n\t"<<states[i]->getName()<<","<<states[i]->getId()<<" -> { ";
        for(auto v :this->states[i]->getSuccessors()){
            cout << v.first << "," << v.second->getName() <<","<<v.second->getId()<< "	";
        }
        cout<<" }";
    }
        cout << endl;
 }


