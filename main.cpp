#include <iostream>
#include "Node.h"
#include "Graph.h"
#include "PathGenerator.h"
#include "or-tools/ortools/linear_solver/linear_solver.h"
#include<fstream>
#include <string>
#include <cstdlib>

bool isNumeric(std::string const &str)
{
    auto it = str.begin();
    while (it != str.end() && std::isdigit(*it)) {
        it++;
    }
    return !str.empty() && it == str.end();
}





int main(int argc, char *argv[]) {


if ( std::string(argv[1]).find("help")!=-1  )
{cout <<"/**************************************************************/\n/********************         help         ********************/ \n -To generate the reachability graph: use the option -r <filename.net>. \n  -To generate the reachability graph then find the optimal solution : use the option -ro <filename.net>. \n -To generate test paths covering the given observable transitions t1,t2...tn: use the option -o <filename.net> <fileobs.txt >. \n -To generate test paths using structural analysis: use the option -a <filename.net>\n -To generate the conplete SOG corresponding to obs trans: use the option -c <filename.net>\n To generate the conplete SOG corresponding to obs trans then find the optimal soution: use the option -co <filename.net>\n-To generate a random graph and extract the optimal solution: give n the state number of the random graph\n -To extract the optimal solution of a .dot file: use the option -d <filename.dot> \n /**************************************************************/"<<endl;
return 0;}
//-To generate the rachability graph of a petri net then extract the optimal solution: use -ro <filename> 
    bool fromfile = false;
    Graph *g = new Graph();
    string nom;
    bool randomgen=isNumeric(argv[1]);  //si on donne un nombre pour la generation aleatoire 
  
    if (randomgen)    // generate a random graph
    {
       g->generateRandomGraph(atoi(argv[1]));
    } 
    else {  //il y a un fichier d'entree 
    
    string option= std::string (argv[1]);
       
   	 if(!option.compare("-d")) 
   	 {
   	  int pp=string(argv[2]).find(".dot"); //position du point
   	 nom=string(argv[2]).substr(0,pp);
   	         cout<<"NOOOM FILE "<<nom<<endl;
 	 cout << "name of the graph file " << endl;
  	 cout << "dot file : " << argv[2] << endl;
   	 g = new Graph(argv[2]);
   	 }
   	 else{
   	 int pp=string(argv[2]).find(".n");
   	 int ps=string(argv[2]).rfind("/");
   	  nom=string(argv[2]).substr(ps+1,pp-ps-1);
   	  string optionSA;
   	  if (!option.find("-c"))optionSA="-c";	//option sans o pour l'approche optimale
   	  else if  (!option.find("-r"))optionSA="-r";   
   	  else optionSA=option; 
   	 string cmd ="./StructAnalysis "+optionSA+" "+string(argv[2]);
   	 std::system(cmd.c_str());  //executer la premiere approche 
   	 string nom_dot;
        if (!option.compare("-co")) nom_dot= "./result/complete_sog_"+nom+".dot";
        else if (!option.compare("-ro")) nom_dot= "./result/reach_"+nom+".dot";
        else return 0;   //ne pas terminer l'approche optimale apres 
        cout<<"the dot file is strored in: "<< nom_dot<<endl;
               g = new Graph(nom_dot);
   	 }
    fromfile = true;
    }






    // print the Graph
    if (!fromfile) g->printGraph();
    else g->printGraphFile();

//    for(auto s : g.states){
//        free(s);
//    }

    // vector of generated paths
    vector<string> generatedPaths,generatedPathsnnull;
    //nb paths
    int nb = 0;

    // generate regex of graph by computing the language of initial node

    vector<transSeqLang> languages = g->computeLanguage(g->initialNode).first;

    while (!languages.empty()) {

        for (string &path: PathGenerator::extractPaths(languages[languages.size() - 1].first)) {
            generatedPaths.push_back(path);
        }
        languages.pop_back();
    }



    // Print all generated paths
    for (int i = 0; i < generatedPaths.size(); i++)
        std::cout << i << ": " << generatedPaths[i]<<endl;

    int nonulp=0; //not null paths
    for (int i = 0; i < generatedPaths.size(); i++)
        if (generatedPaths[i].size()!=0) generatedPathsnnull.push_back(generatedPaths[i]);
    cout<<"nb of no null paths "<< generatedPathsnnull.size()<<endl;

    int constraints[g->transitions.size()][generatedPathsnnull.size() + g->transitions.size()];
    //initialize to 0
    for (int i = 0; i < g->transitions.size(); i++)
    { for (int j = 0; j < generatedPathsnnull.size() + g->transitions.size(); j++) {
            constraints[i][j] = 0;
        }
    }

    string transition;
    string minPathsCoverage[g->transitions.size()];
    string minPath;

    //Charge constraints Matrix with paths from regex and generate minPaths

    for(int i=0; i<generatedPathsnnull.size();i++){
        int pos=-1;   //position de _ pour copier transition
        for(int j=0; j<generatedPathsnnull[i].length(); j++){
            if(generatedPathsnnull[i][j]=='_'){
                transition.clear();
                /*transition.push_back(generatedPaths[i][j-2]);
                transition.push_back(generatedPaths[i][j-1]);
                transition.push_back(generatedPaths[i][j]);*/
                transition=generatedPathsnnull[i].substr(pos+1,j-pos);
                constraints[g->transitions[transition]][i] = 1;
                // To add minimum paths coverage
                minPath = generatedPathsnnull[i].substr(0,j+1);
                if(minPath.size() < generatedPathsnnull[i].size()) {
                    if(minPathsCoverage[g->transitions[transition]].empty() ){
                        minPathsCoverage[g->transitions[transition]] = minPath;
                    }
                    else{
                        if(minPathsCoverage[g->transitions[transition]].size() > minPath.size()){
                            minPathsCoverage[g->transitions[transition]] = minPath;
                        }
                    }
                }
                pos=j;
            }
        }
    }




    //Charge constaraints Matrix with min paths
       //position de _ pour copier transition
    for(int i=0; i<g->transitions.size();i++){
        int pos1=-1;
        for(int j=0; j<minPathsCoverage[i].length(); j++){
            if(minPathsCoverage[i][j]=='_'){
                transition.clear();
                transition=minPathsCoverage[i].substr(pos1+1,j-pos1);
                if (transition!="") {
                    constraints[g->transitions[transition]][i + generatedPathsnnull.size()] = 1;
                }
                pos1=j;

            }
        }
    }
// Print all generated paths
    for (int i = 0; i < g->transitions.size(); i++)
        std::cout << generatedPathsnnull.size()+i << ": " << minPathsCoverage[i] << endl;


    // Create the mip solver with the SCIP backend.
    std::unique_ptr<operations_research::MPSolver> solver(operations_research::MPSolver::CreateSolver("SCIP"));
    if (!solver) {
        LOG(WARNING) << "SCIP solver unavailable.";
        return -1;
    }

    // Create Variables
    std::vector<const operations_research::MPVariable*> x(generatedPathsnnull.size() + g->transitions.size());
    for (int j = 0; j < generatedPathsnnull.size() + g->transitions.size(); ++j) {
        x[j] = solver->MakeIntVar(0.0, 1, "");
    }

    // Create the constraints.
    for (int i = 0; i < g->transitions.size(); ++i) {
        operations_research::MPConstraint* constraint = solver->MakeRowConstraint(1, generatedPathsnnull.size(), "");
        for (int j = 0; j < generatedPathsnnull.size() + g->transitions.size(); ++j) {
            constraint->SetCoefficient(x[j], constraints[i][j]==1?1:0);
        }
    }

    // Create the objective function.
    operations_research::MPObjective* const objective = solver->MutableObjective();
    for (int j = 0; j < generatedPathsnnull.size(); ++j) {
        objective->SetCoefficient(x[j], generatedPathsnnull[j].size());
    }
    for (int j = generatedPathsnnull.size(); j < generatedPathsnnull.size() + g->transitions.size(); ++j) {
        objective->SetCoefficient(x[j], minPathsCoverage[j - generatedPaths.size()].size());
    }

    objective->SetMinimization();

    //solver->set_time_limit(2);
    const operations_research::MPSolver::ResultStatus result_status = solver->Solve();

    // Check that the problem has an optimal solution.
    if (result_status != operations_research::MPSolver::OPTIMAL) {
        cout << "The problem does not have an optimal solution.";
    }
    cout << "Solution:";
    cout << "Optimal objective value = " << objective->Value()<<endl;
    nb = 0;
    if(!randomgen){
    ofstream monFlux("./result/result_opt_"+nom+".txt");

    for (int j = 0; j < generatedPathsnnull.size() + g->transitions.size(); ++j) {
         cout << "x[" << j << "]=" << x[j]->solution_value()<<" // ";
         if(x[j]->solution_value()==1){
             nb++;
             monFlux<<"le chemin optim num "<<nb<<" contient :"<<std::count(generatedPathsnnull[j].begin(),generatedPathsnnull[j].end(),'_')<<endl;

         }
    }
      monFlux<<"le nombre de chemins observés: "<<nb<<endl;
    }
    cout << endl;

    cout << "number of optimal paths: " <<nb << endl;

    cout << "Statistics : ";
    cout << solver->iterations();

  
    return 0;
}
